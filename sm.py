import collections
import os
import socket
import time
import select

NS_SERVER = 'ns-server.epita.fr'
NS_PORT = 4242

ING1_PROMO = 'epita_2016'

class User(object):
  def __init__(self, line):
    fields = line.split()
    self.login = fields[1]
    self.ip = fields[2]
    self.promo = fields[9]

  @property
  def sm(self):
    if self.ip.startswith('10.41'):
      return "sm random"
    else:
      return None

  def __cmp__(self, other):
    return cmp(self.login, other.login)

  def __hash__(self):
    return hash(self.login)

def connect_to_ns(server, port):
  s = socket.socket()
  s.connect((server, port))
  s.setblocking(0)
  ready = select.select([s], [], [], 0.5)
  if ready[0]:
    s.recv(8192) # salut ...
  else:
    return None
  return s

def list_users(sock):
  sock.send(b"list_users\n")
  buf = ''
  while True:
    sock.setblocking(0)
    ready = select.select([sock], [], [], 0.5)
    if ready[0]:
      tmp = sock.recv(8192)
    else:
      return []
    buf += tmp.decode('utf-8')
    if b'\nrep 002' in tmp or tmp == b'':
      break
  return buf.split('\n')[:-2]

def nb_connected():
  sock = connect_to_ns(NS_SERVER, NS_PORT)
  if sock is None:
    return 0
  users = (User(l) for l in list_users(sock))
  promo = (u for u in users if u.promo == ING1_PROMO)
  promo_in_sm = (u for u in promo if u.sm is not None)
  return len(list(promo_in_sm))
