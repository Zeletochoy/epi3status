#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json
import ratp
import sm
import os
from os.path import expanduser
import time

def print_line(message):
    """ Non-buffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()

def read_line():
    """ Interrupted respecting reader for stdin. """
    # try reading a line, removing any extra whitespace
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line
        if not line:
            sys.exit(3)
        return line
    # exit on ctrl-c
    except KeyboardInterrupt:
        sys.exit()

def inter_color(p, c1, c2):
  r1, g1, b1 = int(c1[1:3], 16), int(c1[3:5], 16), int(c1[5:7], 16)
  r2, g2, b2 = int(c2[1:3], 16), int(c2[3:5], 16), int(c2[5:7], 16)
  dr, dg, db = r2 - r1, g2 - g1, b2 - b1
  r, g, b = r1 + p * dr, g1 + p * dg, b1 + p * db
  #print("inter_color: p={}, rgb1={}, rgb2={}, drgb={}, rgb={}"\
  #    .format(p, (r1, g1, b1), (r2, g2, b2), (dr, dg, db), (r, g, b)))
  #print('#%02X%02X%02X' % (r, g, b))
  return '#%02X%02X%02X' % (r, g, b)

def next131():
  filename = expanduser("~") + "/.i3/next131"
  f = open(filename, "r")
  if time.time() - os.stat(filename).st_mtime > 30:
    f.close()
    f = open(filename, "w+")
    new = ratp.lines()['131'].find_station('salengro').schedule()
    f.write("{0} -> {1}\n".format(new[0][1], new[1][1]))
  nextbus = f.read()[:-1]
  try:
    percent = min(1, max(0, int(nextbus.split()[0]) - 5) / 5.)
  except Exception:
    percent = 0
  color = inter_color(percent, "#FF0033", "#0033CC")
  f.close()
  return {'full_text': 'Next 131: {0}'.format(nextbus), 'name': '131', 'color': color}

def nb_in_sm():
  filename = expanduser("~") + "/.i3/sm_nb"
  f = open(filename, "r")
  if time.time() - os.stat(filename).st_mtime > 60:
    f.close()
    f = open(filename, "w+")
    nb = sm.nb_connected()
    f.write("{0}\n".format(nb))
  pop = f.read()[:-1]
  f.close()
  percent = min(1, int(pop) / 300.) if pop else 0

  color = inter_color(percent, "#6600FF", "#4DBD33")
  return {'full_text': 'SM: {0}'.format(pop), 'name': 'sm', 'color': color}

if __name__ == '__main__':
    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains the start of the infinite array.
    print_line(read_line())

    while True:
        line, prefix = read_line(), ''
        # ignore comma at start of lines
        if line.startswith(','):
            line, prefix = line[1:], ','
        j = json.loads(line)
        # insert information into the start of the json, but could be anywhere
        for e in j:
            if e["name"] == "cpu_usage":
                percent = int(''.join(c for c in e["full_text"] if c.isdigit())) / 100.
                e["color"] = inter_color(percent, "#4DBD33", "#FF0033")
        j.insert(0, next131())
        j.insert(0, nb_in_sm())
        # and echo back new encoded json
        print_line(prefix+json.dumps(j))

