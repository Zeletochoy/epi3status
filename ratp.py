#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import requests
import functools
import unicodedata

URLAPI = 'http://metro.breizh.im/dev/ratp_api.php'


def normalize(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn').lower()


class Api:
    def query(**kwargs):
      try:
        kwargs["timeout"] = 0.5
        return requests.get(URLAPI, params=kwargs).json()
      except:
        return None

    line_list = functools.partial(query, action='getLineList')
    direction_list = functools.partial(query, action='getDirectionList')
    station_list = functools.partial(query, action='getStationList')
    schedule = functools.partial(query, action='getSchedule')


class Line:
    def __init__(self, json):
        self.id = int(json['id'])
        self.name = json['line']
        self.type_id = int(json['type_id'])
        self.type_name = json['type_name']
        self.dirs_id = None
        self.dirs = None
        self.sts = None

    def retrieve_directions(self):
        r = Api.direction_list(line=self.id)['directions'][0]
        self.dirs_id = int(r['id'])
        self.dirs = r['direction'].split(' - ')[0].split(' / ')

    def retrieve_stations(self):
        r = Api.station_list(direction=self.directions_id)['stations']
        self.sts = [Station(i, self) for i in r]

    def find_stations(self, name):
        return [s for s in self.stations
                if normalize(name) in normalize(s.name)]

    def find_station(self, name):
        l = self.find_stations(name)
        if len(l) == 0:
            raise AttributeError("Station not found")
        if len(l) > 1:
            l = map(lambda x: x.name, l)
            raise AttributeError('More than one station matching '
                                 '"{}": {}'.format(name, ', '.join(l)))
        return l[0]

    @property
    def directions(self):
        if not self.dirs:
            self.retrieve_directions()
        return self.dirs

    @property
    def directions_id(self):
        if not self.dirs_id:
            self.retrieve_directions()
        return self.dirs_id

    @property
    def stations(self):
        if not self.sts:
            self.retrieve_stations()
        return self.sts

    def __repr__(self):
        return '<Line {}>'.format(self.name)


class Station:
    def __init__(self, json, parent):
        self.id = int(json['id'])
        self.name = json['station']
        self.line = parent

    def schedule(self):
        r = Api.schedule(line=self.line.id, direction=self.line.directions_id,
                         station=self.id)['schedule']
        return list(map(lambda x: list(x.items())[0], r))

    def __repr__(self):
        return '<Station "{}">'.format(self.name)


def lines():
    l = Api.line_list()['lines']
    return {i['line']: Line(i) for i in l}
