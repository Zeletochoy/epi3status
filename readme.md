epi3status
==========

Replace the call to i3status in your i3 config file (usually .i3.config) by
"i3status | path/to/wrapper.py"
